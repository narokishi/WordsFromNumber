# Laravel WordsFromNumber

A laravel package to transform numbers into words.

## Installation

### Composer

Require this package with composer:
```shell
composer require narokishi/wordsfromnumber
```

### Service Provider
```php
'providers' => [
    ...
    Narokishi\WordsFromNumber\WordsServiceProvider::class,
    ...
];
```

## Usage

### Facade

```php
use Narokishi\WordsFromNumber\Facades\Transformer;
    ...
    Transformer::toWords($number, $code);
    ...
```

### Supported codes

```php
protected $transformers = [
    'pl_PL' => PolishTransformer::class,
];
```

### Supported currencies

Look for this array in dictionaries.
```php
private static $currencies = [];
```
