<?php

namespace Narokishi\WordsFromNumber\Transformers;

use Illuminate\Support\Arr;
use InvalidArgumentException;

class Transformer
{
    protected $transformers = [
        'pl_PL' => PolishTransformer::class,
    ];

    public function toWords($amount, $code, $currency = null)
    {
        $transformer = Arr::get($this->transformers, $code);

        if (is_null($transformer)) {
            throw new InvalidArgumentException("Error Processing Request", 1);
        }

        return (new $transformer)->toWords($amount, $currency);
    }
}
