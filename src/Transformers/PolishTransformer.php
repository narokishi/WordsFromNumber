<?php

namespace Narokishi\WordsFromNumber\Transformers;

use Narokishi\WordsFromNumber\Dictionaries\PolishDictionary;
use Narokishi\WordsFromNumber\Helpers\Trimmer;

class PolishTransformer
{
    /**
     *
     * @var Dictionary
     */
    protected $dictionary;

    /**
     * Create new Transformer instance.
     *
     * @param  void
     * @return void
     */
    public function __construct()
    {
        $this->dictionary = new PolishDictionary();
    }

    /**
     * Translate given numbers (in hundreds) to words.
     *
     * @param  string $number
     * @return string
     */
    private function transformToWords($number)
    {
        $units    = intval($number % 10);
        $tens     = intval(($number / 10) % 10);
        $hundreds = intval(($number / 100) % 10);
        $words    = [];

        if ($hundreds > 0) {
            $words[] = $this->dictionary->getCompatibleHundred($hundreds);
        }

        if ($tens == 1) {
            $words[] = $this->dictionary->getCompatibleTeen($units);
        }

        if ($tens > 1) {
            $words[] = $this->dictionary->getCompatibleTen($tens);
        }

        if ($units > 0 && $tens != 1) {
            $words[] = $this->dictionary->getCompatibleUnit($units);
        }

        return implode($this->dictionary->getSeparator(), $words);
    }

    /**
     * Merge numbers as words with exponents.
     *
     * @param  mixed $amount
     * @return string
     */
    public function toWords($amount, $currency)
    {
        // Trim number to exponential notation.
        $amount = Trimmer::trim($amount);

        foreach ($amount as $key => $number) {
            // Get number word.
            $words[] = $this->transformToWords($amount[$key]);

            // Get the last key of array.
            $power   = sizeof($amount) - ($key + 1);

            // Get zero if value is between 0 and 1
            if (sizeof($amount) == 2 && $amount[$key] == 0 && $key == 0) {
                $words[] = $this->dictionary->getZero();
            }

            // Get exponent word.
            $words[] = $this->dictionary->getCompatibleExponent($power, $number);

            // Get currency word.
            switch ($power) {
                case 0:
                    $words[] = $this->dictionary->getCompatibleCurrency($currency, $number, false);
                    break;
                case 1:
                    $words[] = $this->dictionary->getCompatibleCurrency($currency, $number);
                    break;
            }
        }

        return implode($this->dictionary->getSeparator(), array_filter($words));
    }
}
