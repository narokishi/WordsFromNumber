<?php

namespace Narokishi\WordsFromNumber;

use Narokishi\WordsFromNumber\Transformers\Transformer;
use Illuminate\Support\ServiceProvider;

class WordsServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('transformer', function () {
            return new Transformer;
        });
    }
}
