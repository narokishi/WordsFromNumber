<?php

namespace Narokishi\WordsFromNumber\Helpers;

use InvalidArgumentException;
use Illuminate\Support\Str;

class Validator
{
    /**
     * Validate given value and returns it as integer value.
     *
     * @param  mixed
     * @return integer
     */
    public static function validate($value)
    {
        if (!is_scalar($value || is_bool($value))) {
            throw new InvalidArgumentException('Argument is not scalar.');
        }

        $value = Str::replaceFirst(',', '.', $value);

        if (!is_numeric($value)) {
            throw new InvalidArgumentException('Numeric value expected.');
        }

        return Str::replaceFirst('.', '', $value);
    }
}
