<?php

namespace Narokishi\WordsFromNumber\Helpers;

use Narokishi\WordsFromNumber\Helpers\Validator;
use Illuminate\Support\Str;

class Trimmer
{
    /**
     * Trim a given number to exponential notation.
     *
     * @param  integer $number
     * @return array
     */
    public static function trim($number)
    {
        // Validate given number
        $number = Validator::validate($number);

        // Get pennies and remove them from string.
        $threefold = [Str::substr($number, -2)];
        $number    = Str::substr($number, 0, -2);

        // Chunk reversed string into chunks with maximum 3 digits.
        $threefold = array_merge($threefold, array_map('strrev', str_split(strrev($number), 3)));

        // Return words in left to right direction.
        return array_reverse($threefold);
    }
}
