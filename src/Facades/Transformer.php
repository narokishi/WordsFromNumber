<?php

namespace Narokishi\WordsFromNumber\Facades;

use Illuminate\Support\Facades\Facade;

/**
 *
 *
 * @see WordsFromNumber/Transformers/Transformer
 */
class Transformer extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'transformer';
    }
}
