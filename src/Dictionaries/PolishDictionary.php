<?php

namespace Narokishi\WordsFromNumber\Dictionaries;

class PolishDictionary implements Dictionary
{
    /**
     *
     * @var array $units
     */
    private static $units = [
        '',
        'jeden',
        'dwa',
        'trzy',
        'cztery',
        'pięć',
        'sześć',
        'siedem',
        'osiem',
        'dziewięć',
    ];

    /**
     *
     * @var array $teens
     */
    private static $teens = [
        'dziesięć',
        'jedenaście',
        'dwanaście',
        'trzynaście',
        'czternaście',
        'piętnaście',
        'szesnaście',
        'siedemnaście',
        'osiemnaście',
        'dziewiętnaście',
    ];

    /**
     *
     * @var array $tens
     */
    private static $tens = [
        '',
        'dziesięć',
        'dwadzieścia',
        'trzydzieści',
        'czterdzieści',
        'pięćdziesiąt',
        'sześćdziesiąt',
        'siedemdziesiąt',
        'osiemdziesiąt',
        'dziewięćdziesiąt',
    ];

    /**
     *
     * @var array $hundreds
     */
    private static $hundreds = [
        '',
        'sto',
        'dwieście',
        'trzysta',
        'czterysta',
        'pięćset',
        'sześćset',
        'siedemset',
        'osiemset',
        'dziewięćset',
    ];

    /**
     *
     * @var array $exponent
     */
    private static $exponent = [
        ['', '', ''],
        ['', '', ''],
        ['tysiąc', 'tysiące', 'tysięcy'],
        ['milion', 'miliony', 'milionów'],
        ['miliard', 'miliardy', 'miliardów'],
        ['bilion', 'biliony', 'bilionów'],
        ['biliard', 'biliardy', 'biliardów'],
        ['trylion', 'tryliony', 'trylionów'],
        ['tryliard', 'tryliardy', 'tryliardów'],
        ['kwadrylion', 'kwadryliony', 'kwadrylionów'],
        ['kwadryliard', 'kwadryliardy', 'kwadryliardów'],
        ['kwintylion', 'kwintyliony', 'kwintylionów'],
        ['kwintyliiard', 'kwintyliardy', 'kwintyliardów'],
        ['sekstylion', 'sekstyliony', 'sekstylionów'],
        ['sekstyliard', 'sekstyliardy', 'sekstyliardów'],
        ['septylion', 'septyliony', 'septylionów'],
        ['septyliard', 'septyliardy', 'septyliardów'],
        ['oktylion', 'oktyliony', 'oktylionów'],
        ['oktyliard', 'oktyliardy', 'oktyliardów'],
        ['nonylion', 'nonyliony', 'nonylionów'],
        ['nonyliard', 'nonyliardy', 'nonyliardów'],
        ['decylion', 'decyliony', 'decylionów'],
        ['decyliard', 'decyliardy', 'decyliardów'],
    ];

    /**
     *
     * @var array $currencies
     */
    private static $currencies = [
        // Europe
        'ADD' => [
            ['diner', 'dinery', 'dinerów'],
            ['centym', 'centymy', 'centymów'],
        ], // Andora
        'ALL' => [
            ['lek albański', 'leki albańskie', 'leków albańskich'],
            ['qindarka', 'qindarek', 'qindarków'],
        ], // Albania
        'ATS' => [
            ['szyling austriacki', 'szylingi austriackie', 'szylingów austriackich'],
            ['grosz', 'grosze', 'groszy'],
        ], // Austria
        'BAM' => [
            ['marka transferowa', 'marki transferowe', 'marek transferowych'],
            ['fenig', 'fenigi', 'fenigów'],
        ], // Bośnia i Harcegowina
        'BEF' => [
            ['frank belgijski', 'franki belgijskie', 'franków belgijskich'],
            ['centym', 'centymy', 'centymów'],
        ], // Belgia
        'BGN' => [
            ['lew', 'lewa', 'lewów'],
            ['stotinek', 'stotinki', 'stotinków'],
        ], // Bułgaria
        'BYR' => [
            ['rubel', 'ruble', 'rubli'],
            ['kopiejka', 'kopiejki', 'kopiejek'],
        ], // Białoruś
        'CHF' => [
            ['frank szwajcarski', 'franki szwajcarskie', 'franków szwajcarskich'],
            ['centym', 'centymy', 'centymów'],
        ], // Liechtenstein, Szwajcaria
        'CSD' => [
            ['dinar serbski', 'dinary serbskie', 'dinarów serbskich'],
            ['para', 'pary', 'par'],
        ], // Serbia
        'CZK' => [
            ['korona czeska', 'korony czeskie', 'koron czeskich'],
            ['haler', 'halery', 'halerów'],
        ], // Czechy
        'DEM' => [
            ['marka niemiecka', 'marki niemieckie', 'marek niemieckich'],
            ['fenig', 'fenigi', 'fenigów'],
        ], // Niemcy
        'DKK' => [
            ['korona duńska', 'korony duńskie', 'koron duńskich'],
            ['øre', 'øre', 'øre'],
        ], // Dania
        'EUR' => [
            ['euro', 'euro', 'euro'],
            ['cent', 'centy', 'centów'],
        ], /* Andora, Czarnogóra, Estonia,
              Finlandia, Francja, Grecja,
              Hiszpania, Holandia, Irlandia,
              Kosowo, Litwa, Luksemburg,
              Łotwa, Malta, Monako,
              Portugalia, San Marino, Słowacja,
              Słowacja, Słowenia, Watykan,
              Włochy
            */
        'EKK' => [
            ['korona estonska', 'korony estońskie', 'koron estońskich'],
            ['senti', 'senti', 'senti']
        ], // Estonia
        'ESP' => [
            ['peseta hiszpańska', 'pesety hiszpańskie', 'peset hiszpańskich'],
            ['centym', 'centymy', 'centymów'],
        ], // Andora, Hiszpania
        'FIM' => [
            ['marka fińska', 'marki fińskie', 'marek fińskich'],
            ['penni', 'penni', 'penni'],
        ], // Finlandia
        'FRF' => [
            ['frank francuski', 'franki francuskie', 'franków francuskich'],
            ['centym', 'centymy', 'centymów'],
        ], // Andora, Francja, Monako
        'GBP' => [
            ['funt szterling', 'funty szterlingi', 'funtów szterlingów'],
            ['pens', 'pensy', 'pensów'],
        ],
        'GRD' => [
            ['drachma', 'drachma', 'drachma'],
            ['lepta', 'lepta', 'lepta'],
        ], // Grecja
        'HRK' => [
            ['kuna', 'kuny', 'kun'],
            ['lipa', 'lipy', 'lip'],
        ], // Chorwacja
        'HUF' => [
            ['forint', 'forinty', 'forintów'],
            ['filler', 'fillery', 'fillerów'],
        ], // Węgry
        'IEP' => [
            ['funt irlandzki', 'funty irlandzkie', 'funtów irlandzkich'],
            ['pens', 'pensy', 'pensów'],
        ], // Irlandia
        'ISK' => [
            ['korona islandzka', 'korony islandzkie', 'koron islandzkich'],
            ['aurar', 'aurar', 'aurar'],
        ], // Islandia
        'ITL' => [
            ['lir włoski', 'liry włoskie', 'lirów włoskich'],
            ['centesim', 'centesimy', 'centesimów'],
        ], // San Marino, Watykan, Włochy
        'LTL' => [
            ['lit', 'lity', 'litów'],
            ['cent', 'centy', 'centów'],
        ], // Litwa
        'LUF' => [
            ['frank luksemburski', 'franki luksemburskie', 'franków luksemburskich'],
            ['centym', 'centymy', 'centymów'],
        ], // Luksemburg
        'LVL' => [
            ['łat', 'łaty', 'łatów'],
            ['santim', 'santimy', 'santimów'],
        ], // Łotwa
        'MDL' => [
            ['lej mołdawski', 'leje mołdawskie', 'lejów mołdawskich'],
            ['ban', 'bany', 'banów'],
        ], // Mołdawia
        'MTL' => [
            ['lira małtańskia', 'liry małtańskie', 'lir małtańskich'],
            ['cent', 'centy', 'centów'],
        ], // Malta
        'MKD' => [
            ['denar macedoński', 'denary macedońskie', 'denarów macedońskich'],
            ['deni', 'deni', 'deni'],
        ], // Macedonia
        'NLG' => [
            ['gulden floren', 'gulden floreny', 'gulden florenów'],
            ['cent', 'centy', 'centów'],
        ], // Holandia
        'NOK' => [
            ['korona norweska', 'korony norweskie', 'koron norweskich'],
            ['øre', 'øre', 'øre'],
        ], // Norwegia
        'PLN' => [
            ['złoty', 'złote', 'złotych'],
            ['grosz', 'grosze', 'groszy'],
        ], // Polska
        'PLZ' => [
            ['złoty', 'złote', 'złotych'],
            ['grosz', 'grosze', 'groszy'],
        ], // Polska
        'PTE' => [
            ['escudo portugalskie', 'escudo portugalskie', 'escudo portugalskich'],
            ['cantavos', 'cantavos', 'cantavos'],
        ], // Portugalia
        'RON' => [
            ['lej', 'leje', 'lejów'],
            ['ban', 'bany', 'banów'],
        ], // Rumunia
        'RUB' => [
            ['rubel', 'ruble', 'rubli'],
            ['kopiejka', 'kopiejki', 'kopiejek'],
        ], // Rosja
        'SEK' => [
            ['korona szwedzka', 'korony szwedzkie', 'koron szwedzkich'],
            ['halerz', 'halerze', 'halerzy'],
        ], // Słowacja
        'SIT' => [
            ['tolar', 'tolary', 'tolarów'],
            ['stotinov', 'stotinov', 'stotinov'],
        ], // Słowenia
        'SKK' => [
            ['korona szwedzka', 'korony szwedzkie', 'koron szwedzkich'],
            ['öre', 'öre', 'öre'],
        ], // Szwecja
        'UAH' => [
            ['hrywna', 'hrywny', 'hrywn'],
            ['kopiejka', 'kopiejki', 'kopiejek'],
        ], // Ukraina
        // Others
        'AED' => [
            ['dirham', 'dirhamy', 'dirhamów'],
            ['fils', 'filsy', 'filsów'],
        ],
        'AFN' => [
            ['afgani', 'afgani', 'afgani'],
            ['pula', 'pule', 'pul'],
        ],
        'GEL' => [
            ['lari', 'lari', 'lari'],
            ['tetri', 'tetri', 'tetri'],
        ],
        'KES' => [
            ['szyling kenijski', 'szylingi kenijskie', 'szylingów kenijskich'],
            ['centym', 'centymy', 'centymów'],
        ],
        'KMF' => [
            ['frank CFA', 'franki CFA', 'franków CFA'],
            ['centym', 'centymy', 'centymów'],
        ],
    ];

    /**
     *
     * @var string $wordsSeparator
     */
    private static $wordsSeparator = ' ';

    /**
     * Get word separator.
     *
     * @param  void
     * @return string
     */
    public function getSeparator()
    {
        return self::$wordsSeparator;
    }

    /**
     * Get zero word.
     *
     * @param  void
     * @return string
     */
    public function getZero()
    {
        return 'zero';
    }

    /**
     * Get currency word.
     *
     * @param  string  $currency
     * @param  integer $amount
     * @param  boolean $main
     * @return string
     */
    public function getCompatibleCurrency($currency, $amount, $main = true)
    {
        $level = $main ? 0 : 1;
        if (!$main && $amount == 0) {
            return null;
        }

        if (!isset(self::$currencies[$currency])) {
            $currency = 'PLN';
        }

        return self::$currencies[$currency][$level][$this->getVariety($amount)];
    }

    /**
     * Get unit word.
     *
     * @param  integer $unit
     * @return string
     */
    public function getCompatibleUnit($unit)
    {
        return self::$units[$unit];
    }

    /**
     * Get teen word.
     *
     * @param  integer $teen
     * @return string
     */
    public function getCompatibleTeen($teen)
    {
        return self::$teens[$teen];
    }

    /**
     * Get ten word.
     *
     * @param  integer $ten
     * @return string
     */
    public function getCompatibleTen($ten)
    {
        return self::$tens[$ten];
    }

    /**
     * Get hundred word.
     *
     * @param  integer $hundred
     * @return string
     */
    public function getCompatibleHundred($hundred)
    {
        return self::$hundreds[$hundred];
    }

    /**
     * Get exponent word.
     *
     * @param  integer $power
     * @param  integer $number
     * @return string
     */
    public function getCompatibleExponent($power, $number)
    {
        return self::$exponent[$power][$this->getVariety($number)];
    }

    /**
     * Except expression "1 złoty" the following rule applies:
     * For numbers between 5 and 14 or when last digit is: 1, 5, 6, 7, 8, 9, 0 - "złotych" (eg 13, 95 złotych).
     * Last digit is: 2, 3, 4 - "złote" (eg 33, 102 złote).
     * @source http://docs.translatehouse.org/projects/localization-guide/en/latest/l10n/pluralforms.html
     *
     * @param  integer|float $n
     * @return integer
     */
    private function getVariety($n)
    {
        return $n==1 ? 0 : ($n%10>=2 && $n%10<=4 && ($n%100<10 || $n%100>=20) ? 1 : 2);
    }
}
