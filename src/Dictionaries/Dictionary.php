<?php

namespace Narokishi\WordsFromNumber\Dictionaries;

interface Dictionary
{
    /**
     *
     * @param  void
     * @return string
     */
    public function getSeparator();

    /**
     *
     * @param  void
     * @return string
     */
    public function getZero();

    /**
     *
     * @param  string $currency
     * @param  integer $amount
     * @param  boolean $main
     * @return string
     */
    public function getCompatibleCurrency($currency, $amount, $main = true);

    /**
     *
     * @param  integer $unit
     * @return string
     */
    public function getCompatibleUnit($unit);

    /**
     *
     * @param  integer $teen
     * @return string
     */
    public function getCompatibleTeen($teen);

    /**
     *
     * @param  integer $ten
     * @return string
     */
    public function getCompatibleTen($ten);

    /**
     *
     * @param  integer $hundred
     * @return string
     */
    public function getCompatibleHundred($hundred);

    /**
     *
     * @param  integer $power
     * @param  integer $number
     * @return string
     */
    public function getCompatibleExponent($power, $number);
}
